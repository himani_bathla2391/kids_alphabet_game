//
//  ViewController2.m
//  learn alphabets
//
//  Created by Clicklabs 104 on 10/6/15.
//  Copyright (c) 2015 cl. All rights reserved.
//

#import "ViewController2.h"
#import "ViewController3.h"
@interface ViewController2 ()
@property (weak, nonatomic) IBOutlet UIButton *b;
@property (weak, nonatomic) IBOutlet UILabel *ball;
@property (weak, nonatomic) IBOutlet UIImageView *image2;
@property (weak, nonatomic) IBOutlet UIButton *next2;

@end

@implementation ViewController2
UIImage *b1;
UIImage *b2;
UIImage *b3;
UIImage *b4;
UIImage *b5;
@synthesize b;
@synthesize ball;
@synthesize image2;
@synthesize next2;
ViewController3 *c;
- (void)viewDidLoad {
    [super viewDidLoad];
    b1=[UIImage imageNamed:@"b1.tiff"];
    b2=[UIImage imageNamed:@"b2.tiff"];
    b3=[UIImage imageNamed:@"b3.tiff"];
    b4=[UIImage imageNamed:@"b4.tiff"];
    b5=[UIImage imageNamed:@"b5.tiff"];
    self.view.backgroundColor=[UIColor greenColor];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)b:(id)sender {
    image2.animationImages = [NSArray arrayWithObjects:b1,b2,b3,b4,b5,nil];
    image2.animationDuration = 1.5;
    [image2 startAnimating];

}

- (IBAction)next2:(id)sender {
    c= [[ViewController3 alloc]init];
    [self.navigationController pushViewController:c=[[ViewController3 alloc]init] animated:YES];
    
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
