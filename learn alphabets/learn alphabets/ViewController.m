//
//  ViewController.m
//  learn alphabets
//
//  Created by Clicklabs 104 on 10/6/15.
//  Copyright (c) 2015 cl. All rights reserved.
//

#import "ViewController.h"
#import "ViewController2.h"
@interface ViewController ()
@property (weak, nonatomic) IBOutlet UIButton *a;
@property (weak, nonatomic) IBOutlet UILabel *apple;
@property (weak, nonatomic) IBOutlet UIImageView *image1;
@property (weak, nonatomic) IBOutlet UIButton *next;

@end

@implementation ViewController
@synthesize a;
@synthesize apple;
@synthesize image1;
@synthesize next;
UIImage *apl;
UIImage *apl2;
ViewController2 *b;

- (void)viewDidLoad {
    [super viewDidLoad];
    apl= [UIImage imageNamed:@"animated-apple-image-0011-2 (dragged).tiff"];
    apl2= [UIImage imageNamed:@"danceapple.png"];
   
    // Do any additional setup after loading the view, typically from a nib.
}
- (IBAction)a:(id)sender {
    
    image1.animationImages = [NSArray arrayWithObjects:apl,apl2,nil];
    image1.animationDuration = 1.5;
    [image1 startAnimating];
}

- (IBAction)next:(id)sender {
    b= [[ViewController2 alloc]init];
    [self.navigationController pushViewController:b=[[ViewController2 alloc]init] animated:YES];
     
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
